package com.example.root.ubn_radio.view.activities.view.activities;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import com.example.root.ubn_radio.R;
import com.example.root.ubn_radio.view.activities.api.WebServiceResult;
import com.example.root.ubn_radio.view.activities.model.Watch_live.Watchlive;
import com.example.root.ubn_radio.view.activities.utilities.NetworkChecker;
import com.example.root.ubn_radio.view.activities.view.adapter.watchliveadapter;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class Watch_LIve extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener{
    static Watch_LIve instance;
    @BindView(R.id.rv_watchlive)
    RecyclerView rv_watchlive;
    @BindView(R.id.progresslayout)
    RelativeLayout rl_watchlive;
    @BindView(R.id.pulltorefresh)
    SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.progressbar)
    ProgressBar progressBar;

    public static Watch_LIve getInstance(){
        return  instance;
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_watch__live);
        ButterKnife.bind(this);
        instance=this;
        swipeRefreshLayout.setOnRefreshListener(this);
        rl_watchlive.setVisibility(View.VISIBLE);
        WebServiceResult.getShowstowatch("1");
    }
    public void updateserverresponse(Watchlive body){
        if(body!=null){
            if(body.getResponse().getChannels().size()>0){
                LinearLayoutManager manager=new LinearLayoutManager(Watch_LIve.this,LinearLayoutManager.VERTICAL,false);
                rv_watchlive.setLayoutManager(manager);
                rv_watchlive.setAdapter(new watchliveadapter(Watch_LIve.this,body));
                rl_watchlive.setVisibility(View.GONE);
                swipeRefreshLayout.setRefreshing(false);
                progressBar.setVisibility(View.GONE);
            }else{
                rl_watchlive.setVisibility(View.GONE);
                progressBar.setVisibility(View.GONE);
                swipeRefreshLayout.setRefreshing(false);
                noshowsdialopg();
            }

        }else{
            swipeRefreshLayout.setRefreshing(false);
            NetworkChecker networkChecker=new NetworkChecker(Watch_LIve.this);
            if(!networkChecker.isNetworkAvailable(this)){
                progressBar.setVisibility(View.GONE);
                networkChecker.showalertdialog();
            }else{
                progressBar.setVisibility(View.GONE);
                noshowsdialopg();
            }
        }
    }
    @OnClick(R.id.iv_back)
    public void openmenu(){
        super.onBackPressed();
        overridePendingTransition(0,0);
    }
    public void noshowsdialopg(){
        AlertDialog.Builder b = new AlertDialog.Builder(Watch_LIve.this);
        b.setMessage(this.getString(R.string.noliveshowmessage));
        b.setTitle("Watch Live");
        b.setCancelable(false);
        b.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                finish();
                overridePendingTransition(0,0);
            }
        });
        b.show();
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransition(0,0);
    }

    @Override
    public void onRefresh() {
        progressBar.setVisibility(View.VISIBLE);
        WebServiceResult.getShowstowatch("1");
    }
}
