package com.example.root.ubn_radio.view.activities.api;

import com.example.root.ubn_radio.view.activities.model.Listen_live.ListenLive;
import com.example.root.ubn_radio.view.activities.model.Watch_live.Watchlive;
import com.example.root.ubn_radio.view.activities.model.podcast.Podcasts;
import com.example.root.ubn_radio.view.activities.view.activities.Contact;
import com.example.root.ubn_radio.view.activities.view.activities.LIsten_Live;
import com.example.root.ubn_radio.view.activities.view.activities.Podcast;
import com.example.root.ubn_radio.view.activities.view.activities.Shows;
import com.example.root.ubn_radio.view.activities.view.activities.Watch_LIve;

import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Created by root on 6/9/16.
 */
public class WebServiceResult  {
    public static void getChannels(String page){
        WebServiceConnection webServiceConnection=WebServiceConnection.getInstance();
        webServiceConnection.holder.getchannels(page).enqueue(new Callback<ListenLive>() {
            @Override
            public void onResponse(Response<ListenLive> response, Retrofit retrofit) {
                LIsten_Live.getInstance().updateserverresponse(response.body());

            }
            @Override
            public void onFailure(Throwable t) {
                LIsten_Live.getInstance().updateserverresponse(null);
            }
        });
    }
    public static void getShows(String page){
        WebServiceConnection webServiceConnection=WebServiceConnection.getInstance();
        webServiceConnection.holder.getshows(page).enqueue(new Callback<com.example.root.ubn_radio.view.activities.model.Shows.Shows>() {
            @Override
            public void onResponse(Response<com.example.root.ubn_radio.view.activities.model.Shows.Shows> response, Retrofit retrofit) {
                Shows.getInstance().updateserverresponse(response.body());
            }
            @Override
            public void onFailure(Throwable t) {
                Shows.getInstance().updateserverresponse(null);
            }
        });
    }
    public static void getShowstowatch(String page){
        WebServiceConnection webServiceConnection=WebServiceConnection.getInstance();
        webServiceConnection.holder.getshowstowatch(page).enqueue(new Callback<Watchlive>() {
            @Override
            public void onResponse(Response<Watchlive> response, Retrofit retrofit) {
                Watch_LIve.getInstance().updateserverresponse(response.body());
            }
            @Override
            public void onFailure(Throwable t) {
                Watch_LIve.getInstance().updateserverresponse(null);
            }
        });
    }
    public static void getPodcasts(String showid){
        WebServiceConnection webServiceConnection=WebServiceConnection.getInstance();
        webServiceConnection.holder.getpodcasts(showid).enqueue(new Callback<Podcasts>() {
            @Override
            public void onResponse(Response<Podcasts> response, Retrofit retrofit) {
                Podcast.getinstance().updateserverresponse(response.body());
            }
            @Override
            public void onFailure(Throwable t) {
                Podcast.getinstance().updateserverresponse(null);
            }
        });
    }
    public static void postMessage(String name,String email,String subject,String message){
        WebServiceConnection webServiceConnection=WebServiceConnection.getInstance();
        webServiceConnection.holder.postmessage(name,email,subject,message).enqueue(new Callback<com.example.root.ubn_radio.view.activities.model.contact.Contact>() {
            @Override
            public void onResponse(Response<com.example.root.ubn_radio.view.activities.model.contact.Contact> response, Retrofit retrofit) {
                if(response.body().getResponse().equalsIgnoreCase("Mail successfully sent")){
                    Contact.getInstance().updateserverresponse();
                }
            }

            @Override
            public void onFailure(Throwable t) {
              System.out.println("fail");
            }
        });
    }
}
