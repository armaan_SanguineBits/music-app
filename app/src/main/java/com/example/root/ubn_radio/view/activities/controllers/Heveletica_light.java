package com.example.root.ubn_radio.view.activities.controllers;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by root on 25/11/16.
 */

public class Heveletica_light extends TextView {
    public Heveletica_light(Context context, AttributeSet attrs) {
        super(context, attrs);
        setTypeface(Typeface.createFromAsset(context.getAssets(), "Helvetica-Light_B.otf"));
    }
}
