package com.example.root.ubn_radio.view.activities.view.activities;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Patterns;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;
import com.example.root.ubn_radio.R;
import com.example.root.ubn_radio.view.activities.api.WebServiceResult;
import com.example.root.ubn_radio.view.activities.controllers.Heveletica_light_edittext;
import com.example.root.ubn_radio.view.activities.utilities.NetworkChecker;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class Contact extends AppCompatActivity {
    @BindView(R.id.et_name)Heveletica_light_edittext  et_name;
    @BindView(R.id.et_email)Heveletica_light_edittext et_email;
    @BindView(R.id.et_subject)Heveletica_light_edittext et_subject;
    @BindView(R.id.et_message)Heveletica_light_edittext et_message;
    @BindView(R.id.progressbar)ProgressBar progressBar;
    @BindView(R.id.btn_submit)Button btn_submit;
    static  Contact instance;
    private NetworkChecker networkChecker;

    public static Contact getInstance(){
        return instance;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact);
        instance=this;
        ButterKnife.bind(this);
        networkChecker=new NetworkChecker(this);
    }
    @OnClick(R.id.btn_submit)
    public void submit(){
        if(et_name.getText().toString().trim().length()==0){
            Toast.makeText(this, "Please enter your name" , Toast.LENGTH_SHORT).show();

        }else if(et_email.getText().toString().trim().length()==0){
            Toast.makeText(this, "please enter your email", Toast.LENGTH_SHORT).show();

        }else if(et_subject.getText().toString().trim().length()==0){
            Toast.makeText(this, "please enter subject", Toast.LENGTH_SHORT).show();

        }else if(et_message.getText().toString().trim().length()==0){
            Toast.makeText(this, "please enter your message", Toast.LENGTH_SHORT).show();
        } else if (!Patterns.EMAIL_ADDRESS.matcher(et_email.getText().toString()).matches()) {
            Toast.makeText(this, "please enter a valid email", Toast.LENGTH_LONG).show();
        }else if (!networkChecker.isNetworkAvailable(Contact.this)) {
            networkChecker.showalertdialog();
        } else{
            hideKeyboard(Contact.this);
            progressBar.setVisibility(View.VISIBLE);
            et_name.setEnabled(false);
            et_email.setEnabled(false);
            et_subject.setEnabled(false);
            et_message.setEnabled(false);
            btn_submit.setEnabled(false);
            WebServiceResult.postMessage(et_name.getText().toString(),et_email.getText().toString(),et_subject.getText().toString(),et_message.getText().toString());
        }
    }
    @OnClick(R.id.iv_back)
    public void openmenu(){
        super.onBackPressed();
        overridePendingTransition(0,0);
    }
    public void updateserverresponse(){
        et_name.setText("");
        et_email.setText("");
        et_message.setText("");
        et_subject.setText("");
        progressBar.setVisibility(View.GONE);
        et_name.setEnabled(true);
        et_email.setEnabled(true);
        et_subject.setEnabled(true);
        et_message.setEnabled(true);
        btn_submit.setEnabled(true);
        Toast.makeText(instance, "Message Submitted Successfully", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransition(0,0);
    }
    public static  void hideKeyboard(Activity activity) {
        View view = activity.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }
}
