package com.example.root.ubn_radio.view.activities.view.activities;

import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.example.root.ubn_radio.R;
import com.example.root.ubn_radio.view.activities.controllers.Heveletica_light;
import com.example.root.ubn_radio.view.activities.controllers.Roboto_regular;
import com.example.root.ubn_radio.view.activities.model.Listen_live.Channel;
import com.example.root.ubn_radio.view.activities.utilities.NotificationHelperRadio;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.os.AsyncTask.THREAD_POOL_EXECUTOR;

public class PlayRadio extends AppCompatActivity implements MediaPlayer.OnPreparedListener {
    private static PlayRadio instance;
    @BindView(R.id.tv_channel_name)Heveletica_light tv_channelname;
    @BindView(R.id.tv_initialtime)Roboto_regular tv_initialtime;
    @BindView(R.id.tv_finaltime1)Roboto_regular tv_finaltime;
    @BindView(R.id.iv_channelimage)ImageView iv_channelimage;
    @BindView(R.id.tv_currentchannelname)Heveletica_light tv_currentchannelname;
    @BindView(R.id.progress_bar)ProgressBar progressBar;
    @BindView(R.id.iv_play)ImageView iv_play;
    List<Channel> channels=new ArrayList<>();
    private int current_channel_position=0;
    public static boolean status=false;
    private Handler handler;
    private String currenturl="";
    private int timeelapsed=0;
    private Runnable runnable;
    private Intent intent;
    private MediaPlayer mediaPlayer=null;

    public static PlayRadio getinstance(){
    return instance;
}
    @Override


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_play_radio);
        ButterKnife.bind(this);
        instance=this;
        channels= (List<Channel>)getIntent().getSerializableExtra("channels");
        current_channel_position=getIntent().getIntExtra("position",1);
        tv_channelname.setText(channels.get(current_channel_position).getChannelName());
        tv_currentchannelname.setText(channels.get(current_channel_position).getChannelName());
        startfirstchannel();
    }


    public void startfirstchannel(){
        if(channels.get(current_channel_position).getChannelStream().endsWith(".m3u")){
            new DownloadUrls().executeOnExecutor(THREAD_POOL_EXECUTOR,channels.get(current_channel_position).getChannelStream());
        }else {
            currenturl=channels.get(current_channel_position).getChannelStream();
            playmediaplayer(currenturl);
        }
    }
    public void playmediaplayer(String s){
        if (mediaPlayer==null){
            mediaPlayer=new MediaPlayer();
            try {
                mediaPlayer.setDataSource(this, Uri.parse(s));
                mediaPlayer.setOnPreparedListener(this);
                mediaPlayer.prepareAsync();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }else {
            mediaPlayer.reset();
            try {
                mediaPlayer.setDataSource(this, Uri.parse(s));
            } catch (IOException e) {
                e.printStackTrace();
            }
           mediaPlayer.prepareAsync();
        }

    }
    @OnClick(R.id.iv_back)
    public void openMenu(){
        if (mediaPlayer != null) {
            mediaPlayer.release();
            mediaPlayer=null;
        }
        if(intent!=null){
            stopService(intent);
            intent=null;
        }
        super.onBackPressed();
        overridePendingTransition(0,0);
    }
    @OnClick(R.id.iv_play)
    public void pause(){
                    if(status){
                        iv_play.setImageDrawable(getResources().getDrawable(R.drawable.play));
                        if(!currenturl.equals(""))
                            mediaPlayer.start();
                            status=false;
                    }else{
                        iv_play.setImageDrawable(getResources().getDrawable(R.drawable.pause));
                        if(!currenturl.equals(""))
                            mediaPlayer.pause();
                            status=true;
                    }
        try{
            NotificationHelperRadio.getinstance().buildnotification(status);
        }catch (Exception e){

        }
    }
    @OnClick(R.id.iv_nextchannel)
    public void playnextchannel(){
        try{
            timeelapsed=0;
            tv_initialtime.setText("00:00");
            tv_finaltime.setText("00:00");
            current_channel_position=current_channel_position+1;
            tv_channelname.setText(channels.get(current_channel_position).getChannelName());
            tv_currentchannelname.setText(channels.get(current_channel_position).getChannelName());
            if(channels.get(current_channel_position).getChannelStream().endsWith(".m3u")){
                new DownloadUrls().executeOnExecutor(THREAD_POOL_EXECUTOR,channels.get(current_channel_position).getChannelStream());
            }else {
                currenturl=channels.get(current_channel_position).getChannelStream();
                playmediaplayer(currenturl);
            }
        }catch (IndexOutOfBoundsException e){
        }
    }
    @OnClick(R.id.iv_backchannel)
    public  void playlastchannel(){
        try{
            timeelapsed=0;
            tv_initialtime.setText("00:00");
            tv_finaltime.setText("00:00");
            current_channel_position=current_channel_position-1;
            tv_channelname.setText(channels.get(current_channel_position).getChannelName());
            tv_currentchannelname.setText(channels.get(current_channel_position).getChannelName());
            if(channels.get(current_channel_position).getChannelStream().endsWith(".m3u")){
                new DownloadUrls().executeOnExecutor(THREAD_POOL_EXECUTOR,channels.get(current_channel_position).getChannelStream());
            }else {
                currenturl=channels.get(current_channel_position).getChannelStream();
                playmediaplayer(currenturl);
            }
        }catch (IndexOutOfBoundsException e){

        }
    }
    @Override
    protected void onDestroy() {
        if (mediaPlayer != null) {
           mediaPlayer.release();
            mediaPlayer=null;
        }
        if(intent!=null){
            stopService(intent);
        }
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancel(1);
        super.onDestroy();

    }

    @Override
    public void onBackPressed() {
        if (mediaPlayer != null) {
            mediaPlayer.release();
            mediaPlayer=null;
        }
        if(intent!=null){
            stopService(intent);
            intent=null;
        }
        super.onBackPressed();
    }

    public List<String> readURLs(String url) {
        if(url==null) return null;
        ArrayList<String> allURls = new ArrayList<String>();
        try {
            URL urls = new URL(url);
            HttpURLConnection co=(HttpURLConnection)urls.openConnection();
            co.connect();
            int c=co.getResponseCode();
            Log.e("responsecode",c+"");
            BufferedReader in = new BufferedReader(new InputStreamReader(co.getInputStream()));
            String str;
            while ((str = in.readLine()) != null) {
                allURls.add(str);
            }
            in.close();
            co.disconnect();
            return allURls ;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public void starttimer(){
         final int interval = 1000;
         handler = new Handler();
         runnable = new Runnable(){
            public void run() {
                timeelapsed=timeelapsed+interval;
                DecimalFormat decimalFormat = new DecimalFormat("00");
                String a = decimalFormat.format(((int) (timeelapsed/ 1000) / 60));
                String b = decimalFormat.format((timeelapsed / 1000) % 60);
                tv_initialtime.setText(a+":"+b+"");
                handler.postDelayed(this, interval);
            }
        };
        handler.postDelayed(runnable, interval);
    }
    public void stoptimer(){
        handler.removeCallbacks(runnable);
    }

    @Override
    public void onPrepared(MediaPlayer mp) {
        mp.start();
        intent = new Intent(this, NotificationHelperRadio.class);
        startService(intent);
    }

    public class DownloadUrls extends AsyncTask<String,Void ,List<String>> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }
        @Override
        protected List<String> doInBackground(String... params) {
            return readURLs(params[0]);
        }
        @Override
        protected void onPostExecute(List<String> strings) {
            List<String> list=strings;
                if(strings!=null && strings.size()>0){
                    currenturl=strings.get(0);
                    playmediaplayer(currenturl);

                }
               super.onPostExecute(strings);
        }


    }
}

