package com.example.root.ubn_radio.view.activities.model.podcast;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Podcasts {

@SerializedName("response")
@Expose
private Response response;

/**
*
* @return
* The response
*/
public Response getResponse() {
return response;
}

/**
*
* @param response
* The response
*/
public void setResponse(Response response) {
this.response = response;
}

}