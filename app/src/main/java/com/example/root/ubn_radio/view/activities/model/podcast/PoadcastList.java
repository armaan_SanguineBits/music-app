package com.example.root.ubn_radio.view.activities.model.podcast;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PoadcastList {

@SerializedName("id")
@Expose
private String id;
@SerializedName("show_id")
@Expose
private String showId;
@SerializedName("podcast_name")
@Expose
private String podcastName;
@SerializedName("podcast_url")
@Expose
private String podcastUrl;
    public boolean isPlaying;

    public boolean isPlaying() {
        return isPlaying;
    }

    public void setPlaying(boolean playing) {
        isPlaying = playing;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    private String duration="00:00";



    /**
*
* @return
* The id
*/
public String getId() {
return id;
}

/**
*
* @param id
* The id
*/
public void setId(String id) {
this.id = id;
}

/**
*
* @return
* The showId
*/
public String getShowId() {
return showId;
}

/**
*
* @param showId
* The show_id
*/
public void setShowId(String showId) {
this.showId = showId;
}

/**
*
* @return
* The podcastName
*/
public String getPodcastName() {
return podcastName;
}

/**
*
* @param podcastName
* The podcast_name
*/
public void setPodcastName(String podcastName) {
this.podcastName = podcastName;
}

/**
*
* @return
* The podcastUrl
*/
public String getPodcastUrl() {
return podcastUrl;
}

/**
*
* @param podcastUrl
* The podcast_url
*/
public void setPodcastUrl(String podcastUrl) {
this.podcastUrl = podcastUrl;
}

}