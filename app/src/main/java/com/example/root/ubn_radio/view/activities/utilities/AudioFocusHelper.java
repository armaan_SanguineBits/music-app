package com.example.root.ubn_radio.view.activities.utilities;

import android.content.Context;
import android.media.AudioManager;
import android.widget.Toast;

public class AudioFocusHelper implements AudioManager.OnAudioFocusChangeListener {
    AudioManager mAudioManager;
    Context mContext;



    public AudioFocusHelper(Context ctx) {
        mContext=ctx;
        mAudioManager = (AudioManager)mContext.getSystemService(Context.AUDIO_SERVICE);
    }

    public boolean requestFocus() {
        return AudioManager.AUDIOFOCUS_REQUEST_GRANTED == mAudioManager.requestAudioFocus(this,AudioManager.STREAM_MUSIC, AudioManager.AUDIOFOCUS_GAIN);
    }

    public boolean abandonFocus() {
        return AudioManager.AUDIOFOCUS_REQUEST_GRANTED==mAudioManager.abandonAudioFocus(this);
    }

    @Override
    public void onAudioFocusChange(int focusChange) {
        Toast.makeText(mContext,"focus changed",Toast.LENGTH_SHORT).show();
    }
}