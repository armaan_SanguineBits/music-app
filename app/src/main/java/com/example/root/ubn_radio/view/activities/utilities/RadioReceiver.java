package com.example.root.ubn_radio.view.activities.utilities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;

import com.example.root.ubn_radio.view.activities.view.activities.PlayRadio;
import com.example.root.ubn_radio.view.activities.view.activities.Podcast;


/**
 * Created by root on 20/12/16.
 */

public class RadioReceiver extends BroadcastReceiver implements medaiinterface {

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equalsIgnoreCase("com.example.root.ubn_radio.TOGGLE_PLAYBACK_CHANNEL")) {
            PlayRadio.getinstance().pause();
            NotificationHelperRadio.getinstance().buildnotification(PlayRadio.status);
        }
        if (intent.getAction().equalsIgnoreCase("com.example.root.ubn_radio.PREV_CHANNEL")) {
            PlayRadio.getinstance().playlastchannel();

        }
        if (intent.getAction().equalsIgnoreCase("com.example.root.ubn_radio.NEXT_CHANNEL")) {
            PlayRadio.getinstance().playnextchannel();
        }
    }
}
