package com.example.root.ubn_radio.view.activities.view.activities;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.example.root.ubn_radio.R;
import com.example.root.ubn_radio.view.activities.api.WebServiceResult;
import com.example.root.ubn_radio.view.activities.model.Listen_live.ListenLive;
import com.example.root.ubn_radio.view.activities.utilities.NetworkChecker;
import com.example.root.ubn_radio.view.activities.view.adapter.listenliveadapter;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LIsten_Live extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener{
    static  LIsten_Live instance;
    @BindView(R.id.rv_listenlive)
    RecyclerView rv_listenlive;
    @BindView(R.id.progresslayout)
    RelativeLayout rl_listenlive;
    @BindView(R.id.progressbar)
    ProgressBar progressBar;
    @BindView(R.id.pulltorefresh)
    SwipeRefreshLayout swipeRefreshLayout;
    public static LIsten_Live getInstance(){
        return  instance;
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listen__live);
        ButterKnife.bind(this);
        instance=this;
        swipeRefreshLayout.setOnRefreshListener(this);
        rl_listenlive.setVisibility(View.VISIBLE);
        WebServiceResult.getChannels("1");
    }
    public void updateserverresponse(ListenLive body){
        if(body!=null){
            if(body.getResponse().getChannels().size()>0){
                LinearLayoutManager manager=new LinearLayoutManager(LIsten_Live.this,LinearLayoutManager.VERTICAL,false);
                rv_listenlive.setLayoutManager(manager);
                rv_listenlive.setAdapter(new listenliveadapter(LIsten_Live.this,body));
                rl_listenlive.setVisibility(View.GONE);
                progressBar.setVisibility(View.GONE);
                swipeRefreshLayout.setRefreshing(false);
            }else{
                noshowsdialopg();
                progressBar.setVisibility(View.GONE);
                swipeRefreshLayout.setRefreshing(false);
            }
        }else{
            swipeRefreshLayout.setRefreshing(false);
            NetworkChecker networkChecker=new NetworkChecker(LIsten_Live.this);
            if(!networkChecker.isNetworkAvailable(this)){
                progressBar.setVisibility(View.GONE);
                networkChecker.showalertdialog();
            }else{
                progressBar.setVisibility(View.GONE);
                noshowsdialopg();
            }
        }
    }
    @OnClick(R.id.iv_back)
    public void openmenu(){
        super.onBackPressed();
        overridePendingTransition(0,0);
    }
    public void noshowsdialopg(){
        AlertDialog.Builder b = new AlertDialog.Builder(LIsten_Live.this);
        b.setMessage(this.getString(R.string.nochannelmessage));
        b.setTitle("Listen Live");
        b.setCancelable(false);
        b.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                finish();
                overridePendingTransition(0,0);
            }
        });
        b.show();
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransition(0,0);
    }

    @Override
    public void onRefresh() {
        progressBar.setVisibility(View.VISIBLE);
        WebServiceResult.getChannels("1");
    }
}
