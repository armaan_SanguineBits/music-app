package com.example.root.ubn_radio.view.activities.view.activities;

import android.app.Activity;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import com.example.root.ubn_radio.R;
import com.example.root.ubn_radio.view.activities.api.WebServiceResult;
import com.example.root.ubn_radio.view.activities.model.Shows.ShowsList;
import com.example.root.ubn_radio.view.activities.utilities.NetworkChecker;
import com.example.root.ubn_radio.view.activities.view.adapter.showsadapter;
import java.util.ArrayList;
import java.util.List;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class Shows extends AppCompatActivity {
    static Shows instance;
    @BindView(R.id.rv_shows)
    RecyclerView rv_shows;
    @BindView(R.id.progresslayout)
    RelativeLayout rl_shows;
    @BindView(R.id.progressbar)
    ProgressBar progressBar;
    List<ShowsList> showslist=new ArrayList<>();
    int TotalPages=5;
    String PAGE="1";
    private showsadapter showsadap;
    boolean status=true;
    public static Shows getInstance(){
        return  instance;
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shows);
        instance=this;
        ButterKnife.bind(this);
        rl_shows.setVisibility(View.VISIBLE);
        WebServiceResult.getShows(PAGE);
    }
    public void updateserverresponse(com.example.root.ubn_radio.view.activities.model.Shows.Shows body){
        if(body!=null){
            if(body.getResponse().getShowsList().size()>0){
                showslist.addAll(body.getResponse().getShowsList());
                if(showsadap!=null){
                    showsadap=new showsadapter(Shows.this,showslist);
                    showsadap.notifyDataSetChanged();
                    Parcelable recyclerViewState;
                    recyclerViewState = rv_shows.getLayoutManager().onSaveInstanceState();
                    rv_shows.getLayoutManager().onRestoreInstanceState(recyclerViewState);
                    progressBar.setVisibility(View.GONE);
                    status=true;
                }else{
                    final LinearLayoutManager manager=new LinearLayoutManager(Shows.this,LinearLayoutManager.VERTICAL,false);
                    rv_shows.setLayoutManager(manager);
                    showsadap=new showsadapter(Shows.this,showslist);
                    rv_shows.setAdapter(new showsadapter(Shows.this,showslist));
                    rv_shows.addOnScrollListener(new RecyclerView.OnScrollListener(){
                        @Override
                        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                            if(dy>0){
                                int  visibleItemCount = manager.getChildCount();
                                int totalItemCount = manager.getItemCount();
                                int  pastVisiblesItems = manager.findFirstVisibleItemPosition();
                                    if ((visibleItemCount + pastVisiblesItems)>= totalItemCount){
                                        if(status){
                                            int nextpage=Integer.parseInt(PAGE)+1;
                                            PAGE=nextpage+"";
                                            if(Integer.parseInt(PAGE)<TotalPages){
                                                progressBar.setVisibility(View.VISIBLE);
                                                onLoadMore(PAGE);
                                            }
                                            status=false;
                                        }
                                    }
                            }
                        }
                    });
                }
                rl_shows.setVisibility(View.GONE);
                progressBar.setVisibility(View.GONE);
            }else{
                noshowsdialopg2();
                rl_shows.setVisibility(View.GONE);
                progressBar.setVisibility(View.GONE);
            }
        }else{
            rl_shows.setVisibility(View.GONE);
            progressBar.setVisibility(View.GONE);
            NetworkChecker networkChecker=new NetworkChecker(Shows.this);
            if(!networkChecker.isNetworkAvailable(this)){
                networkChecker.showalertdialog();
            }else{
                if(showslist.size()==0)
                    noshowsdialopg();
                else{
                    noshowsdialopg2();
                }
            }
        }
    }

    private void onLoadMore(String page) {
        WebServiceResult.getShows(page);
    }

    @OnClick(R.id.iv_back)
    public void openmenu(){

        super.onBackPressed();
        overridePendingTransition(0,0);
    }
    public void noshowsdialopg(){
        AlertDialog.Builder b = new AlertDialog.Builder(Shows.this);
        b.setMessage(this.getString(R.string.noshows));
        b.setTitle("Shows");
        b.setCancelable(false);
        b.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                finish();
                overridePendingTransition(0,0);
            }
        });
        if(!((Activity) Shows.this).isFinishing())
        {
            b.show();
        }

    }
    public void noshowsdialopg2(){
        AlertDialog.Builder b = new AlertDialog.Builder(Shows.this);
        b.setMessage(this.getString(R.string.noshows));
        b.setTitle("Shows");
        b.setCancelable(false);
        b.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        b.show();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransition(0,0);
    }
}
