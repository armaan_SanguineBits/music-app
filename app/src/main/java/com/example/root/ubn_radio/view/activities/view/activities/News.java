package com.example.root.ubn_radio.view.activities.view.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import com.example.root.ubn_radio.R;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class News extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news);
        ButterKnife.bind(this);
    }
    @OnClick(R.id.iv_back)
    public void openMenu(){
        onBackPressed();
        overridePendingTransition(0,0);
    }
}
