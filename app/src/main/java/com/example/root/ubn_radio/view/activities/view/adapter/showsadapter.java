package com.example.root.ubn_radio.view.activities.view.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.root.ubn_radio.R;
import com.example.root.ubn_radio.view.activities.controllers.Heveletica_light;
import com.example.root.ubn_radio.view.activities.model.Listen_live.ListenLive;
import com.example.root.ubn_radio.view.activities.model.Shows.Shows;
import com.example.root.ubn_radio.view.activities.model.Shows.ShowsList;
import com.example.root.ubn_radio.view.activities.view.activities.PlayRadio;
import com.example.root.ubn_radio.view.activities.view.activities.Podcast;
import com.squareup.picasso.Picasso;

import java.io.Serializable;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by root on 17/10/16.
 */

public class showsadapter extends RecyclerView.Adapter<showsadapter.viewholder> {
    Activity activity;
    List<ShowsList> showsList;

    public showsadapter(Activity activity, List<ShowsList> showsList) {
        this.activity = activity;
        this.showsList = showsList;
    }

    @Override
    public viewholder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater=(LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v=inflater.inflate(R.layout.showsitemlayout,parent,false);
        return new viewholder(v);
    }

    @Override
    public void onBindViewHolder(viewholder holder, final int position) {
        holder.tv_showtitle.setText(showsList.get(position).getShowTitle());
        holder.tv_authorname.setText(showsList.get(position).getShowDesc());
        holder.rl_next_show.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.startActivity(new Intent(activity, Podcast.class).putExtra("position",position).putExtra("show", (Serializable) showsList.get(position)));
                activity.overridePendingTransition(0,0);
            }
        });
        Picasso.with(activity).load(showsList.get(position).getThumbnailUrl()).into(holder.iv_show);
    }

    @Override
    public int getItemCount() {
        return showsList.size();
    }

    public class viewholder extends RecyclerView.ViewHolder{
        CircleImageView iv_show;
        Heveletica_light tv_showtitle;
        Heveletica_light tv_authorname;
        RelativeLayout rl_next_show;
        public viewholder(View itemView) {
            super(itemView);
            iv_show=(CircleImageView) itemView.findViewById(R.id.iv_show);
            tv_showtitle=(Heveletica_light) itemView.findViewById(R.id.tv_showtitle);
            tv_authorname=(Heveletica_light) itemView.findViewById(R.id.tv_author_name);
            rl_next_show=(RelativeLayout) itemView.findViewById(R.id.rl_next_show);
        }
    }
}
