package com.example.root.ubn_radio.view.activities.utilities;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;

import com.example.root.ubn_radio.R;

/**
 * Created by Mobile on 11/21/2016.
 */

public class NetworkChecker {
   public Activity activity;

    public NetworkChecker(Activity activity) {
        this.activity = activity;
    }

    public  boolean isNetworkAvailable(final Context context) {
        final ConnectivityManager connectivityManager = ((ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE));
        return connectivityManager.getActiveNetworkInfo() != null && connectivityManager.getActiveNetworkInfo().isConnected();
    }
    public  void showalertdialog() {
            AlertDialog.Builder b = new AlertDialog.Builder(activity);
            b.setMessage(activity.getString(R.string.nointernetconnection));
            b.setTitle("Alert");
            b.setCancelable(false);
            b.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    activity.finish();
                    activity.overridePendingTransition(0,0);
                }
            });

            b.show();
        }
}
