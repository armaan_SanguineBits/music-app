package com.example.root.ubn_radio.view.activities.view.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import com.example.root.ubn_radio.R;
import com.example.root.ubn_radio.view.activities.controllers.Heveletica_light;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class Latestnews extends AppCompatActivity  {
    @BindView(R.id.tv_channel_name)
    Heveletica_light tv_channel_name;
    @BindView(R.id.webview) WebView webview;
    @BindView(R.id.pb_webview) ProgressBar pb_webview;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.latestnews_layout);
        ButterKnife.bind(this);
        tv_channel_name.setSelected(true);
        tv_channel_name.setEllipsize(TextUtils.TruncateAt.MARQUEE);
        tv_channel_name.setSingleLine(true);
        WebSettings webSettings = webview.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webview.setWebChromeClient(new WebChromeClient() {
            public void onProgressChanged(WebView view, int progress) {
                pb_webview.setProgress(progress);
            }
        });
        webview.loadUrl("http://ubnradio.com/news/");
        webview.setWebViewClient(new MyWebViewClient());
    }
    private class MyWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            return false;
        }
    }
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK) && webview.canGoBack()) {
            webview.goBack();
            return true;
        }
        // If it wasn't the Back key or there's no web page history, bubble up to the default
        // system behavior (probably exit the activity)
        return super.onKeyDown(keyCode, event);
    }
    @OnClick(R.id.iv_back)
    public void gotomenu(){
        super.onBackPressed();
        overridePendingTransition(0,0);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransition(0,0);
    }
}
