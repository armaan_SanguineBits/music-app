package com.example.root.ubn_radio.view.activities.model.Watch_live;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Channel {

@SerializedName("channel_id")
@Expose
private String channelId;
@SerializedName("channel_name")
@Expose
private String channelName;
@SerializedName("channel_stream")
@Expose
private String channelStream;
@SerializedName("channel_thumbnail_url")
@Expose
private String channelThumbnailUrl;

/**
*
* @return
* The channelId
*/
public String getChannelId() {
return channelId;
}

/**
*
* @param channelId
* The channel_id
*/
public void setChannelId(String channelId) {
this.channelId = channelId;
}

/**
*
* @return
* The channelName
*/
public String getChannelName() {
return channelName;
}

/**
*
* @param channelName
* The channel_name
*/
public void setChannelName(String channelName) {
this.channelName = channelName;
}

/**
*
* @return
* The channelStream
*/
public String getChannelStream() {
return channelStream;
}

/**
*
* @param channelStream
* The channel_stream
*/
public void setChannelStream(String channelStream) {
this.channelStream = channelStream;
}

/**
*
* @return
* The channelThumbnailUrl
*/
public String getChannelThumbnailUrl() {
return channelThumbnailUrl;
}

/**
*
* @param channelThumbnailUrl
* The channel_thumbnail_url
*/
public void setChannelThumbnailUrl(String channelThumbnailUrl) {
this.channelThumbnailUrl = channelThumbnailUrl;
}

}