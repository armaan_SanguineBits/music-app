package com.example.root.ubn_radio.view.activities.view.activities;

import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import com.example.root.ubn_radio.R;
import com.example.root.ubn_radio.view.activities.api.WebServiceResult;
import com.example.root.ubn_radio.view.activities.controllers.Heveletica_light;
import com.example.root.ubn_radio.view.activities.controllers.Roboto_regular;
import com.example.root.ubn_radio.view.activities.model.Shows.ShowsList;
import com.example.root.ubn_radio.view.activities.model.podcast.Podcasts;
import com.example.root.ubn_radio.view.activities.utilities.NetworkChecker;
import com.example.root.ubn_radio.view.activities.utilities.NotificationHelper;
import com.example.root.ubn_radio.view.activities.utilities.medaiinterface;
import com.example.root.ubn_radio.view.activities.view.adapter.podcastadapter;
import com.squareup.picasso.Picasso;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.Observable;
import java.util.Observer;
import java.util.Random;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class Podcast extends AppCompatActivity implements SeekBar.OnSeekBarChangeListener, MediaPlayer.OnErrorListener, MediaPlayer.OnCompletionListener, MediaPlayer.OnBufferingUpdateListener, MediaPlayer.OnPreparedListener, MediaPlayer.OnSeekCompleteListener, medaiinterface, Observer, MediaPlayer.OnInfoListener {
    public static int position = 0;
    @BindView(R.id.progresslayout)
    RelativeLayout progresslayout;
    @BindView(R.id.tv_title_podcast)
    Heveletica_light tv_title_podcast;
    @BindView(R.id.tv_title)
    Heveletica_light tv_title;
    @BindView(R.id.rv_podcast)
    RecyclerView rv_podcast;
    @BindView(R.id.iv_podcast)
    ImageView iv_podcast;
    @BindView(R.id.iv_play)
    ImageView iv_play;
    @BindView(R.id.sb_podcast)
    SeekBar sb_podcast;
    @BindView(R.id.iv_repeat)
    ImageView iv_repeat;
    @BindView(R.id.iv_shuffle)
    ImageView iv_shuffle;
    @BindView(R.id.iv_starttime)
    Roboto_regular tv_initialtime;
    @BindView(R.id.iv_endtime)
    Roboto_regular tv_finaltime;
    private ShowsList Show;
    public static boolean status = true;
    static Podcast instance;
    public static Podcasts podcasts;
    private boolean status2 = false;
    private boolean status3 = false;
    private boolean status4 = false;
    public static podcastadapter podcastadap;
    private LinearLayoutManager linearLayoutManager;
    private Handler handler;
    private Runnable r = null;
    private int position1;
    private Intent intent;

    public static Podcast getinstance() {
        return instance;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_podcast);
        instance = this;
        ButterKnife.bind(this);
        progresshelper.addObserver(this);
        sb_podcast.setOnSeekBarChangeListener(this);
        Show = (ShowsList) getIntent().getSerializableExtra("show");
       tv_title_podcast.setText(Show.getShowTitle());
        Picasso.with(this).load(Show.getThumbnailUrl()).into(iv_podcast);
        WebServiceResult.getPodcasts(Show.getId());
    }

    @OnClick(R.id.iv_play)
    public void play() {
        handlePlayPause();
        try{
            NotificationHelper.getinstance().buildnotification(!Podcast.status);
        }catch (Exception e){

        }
    }

    private void handlePlayPause() {
        if (status) {
            iv_play.setImageDrawable(getResources().getDrawable(R.drawable.play_btn));
            progresshelper.getMediaplayer().pause();
            clearadapter();
            status = false;
        } else {
            iv_play.setImageDrawable(getResources().getDrawable(R.drawable.pause_btn));
            progresshelper.getMediaplayer().start();
            notifyadapter();
            status = true;
        }
    }


    @OnClick(R.id.iv_rewind)
    public void rewind() {
        if (status3) {
            Random r = new Random();
            position = r.nextInt(podcasts.getResponse().getPoadcastList().size() - 1);
            tv_title.setText(podcasts.getResponse().getPoadcastList().get(position).getPodcastName());
            tv_initialtime.setText("00:00");
            tv_finaltime.setText("00:00");
            notifyadapter();
            changesong();
        } else {
            position = position - 1;
            if (!(position < 0)) {
                tv_title.setText(podcasts.getResponse().getPoadcastList().get(position).getPodcastName());
                tv_initialtime.setText("00:00");
                tv_finaltime.setText("00:00");
                notifyadapter();
                changesong();
            }
        }
    }

    @OnClick(R.id.iv_forward)
    public void forward() {
        if (status3) {
            Random r = new Random();
            position = r.nextInt(podcasts.getResponse().getPoadcastList().size() - 1);
        } else {
            position = position + 1;
        }
        if (position == podcasts.getResponse().getPoadcastList().size()) {
            if (status4) {
                position = 0;
                tv_title.setText(podcasts.getResponse().getPoadcastList().get(position).getPodcastName());
                tv_initialtime.setText("00:00");
                tv_finaltime.setText("00:00");
                notifyadapter();
                changesong();
            }
        } else if (position < podcasts.getResponse().getPoadcastList().size()) {
            tv_title.setText(podcasts.getResponse().getPoadcastList().get(position).getPodcastName());
            tv_initialtime.setText("00:00");
            tv_finaltime.setText("00:00");
            notifyadapter();
            changesong();
        }
    }

    @OnClick(R.id.iv_repeat)
    public void repeat() {
        if (!status4) {
            iv_repeat.setImageDrawable(getResources().getDrawable(R.drawable.simple_repeat_white));
            status4 = true;
        } else if (!status2) {
            iv_repeat.setImageDrawable(getResources().getDrawable(R.drawable.repeat_white));
            status2 = true;
        } else {
            iv_repeat.setImageDrawable(getResources().getDrawable(R.drawable.repeat2));
            status2 = false;
            status4 = false;
        }
    }

    @OnClick(R.id.iv_shuffle)
    public void shuffle() {
        if (!status3) {
            iv_shuffle.setImageDrawable(getResources().getDrawable(R.drawable.shuffle_white));
            status3 = true;
        } else {
            iv_shuffle.setImageDrawable(getResources().getDrawable(R.drawable.shuffle_light));
            status3 = false;
        }
    }

    public void repeatPodcasts() {
        tv_initialtime.setText("00:00");
        tv_finaltime.setText("00:00");
        tv_title.setText(podcasts.getResponse().getPoadcastList().get(position).getPodcastName());
        notifyadapter();
        changesong();

    }

    public void shufflePodcasts() {
        Random r = new Random();
        position = r.nextInt(podcasts.getResponse().getPoadcastList().size() - 1);
        tv_title.setText(podcasts.getResponse().getPoadcastList().get(position).getPodcastName());
        tv_initialtime.setText("00:00");
        tv_finaltime.setText("00:00");
        notifyadapter();
        changesong();

    }


    public void changesong() {
        progresshelper.getMediaplayer().reset();
        try {
            progresshelper.getMediaplayer().setDataSource(podcasts.getResponse().getPoadcastList().get(position).getPodcastUrl());
        } catch (IOException e) {
            e.printStackTrace();
        }
        progresshelper.getMediaplayer().prepareAsync();
    }

    public void playfirstsong(String s) throws IOException {
        progresshelper.setMediaplayer(new MediaPlayer());
        progresshelper.getMediaplayer().setAudioStreamType(AudioManager.STREAM_MUSIC);
        progresshelper.getMediaplayer().setDataSource(getApplicationContext(), Uri.parse(s));
        progresshelper.getMediaplayer().setOnPreparedListener(this);
        progresshelper.getMediaplayer().setOnBufferingUpdateListener(this);
        progresshelper.getMediaplayer().setOnSeekCompleteListener(this);
        progresshelper.getMediaplayer().setOnErrorListener(this);
        progresshelper.getMediaplayer().setOnCompletionListener(this);
        progresshelper.getMediaplayer().prepareAsync();
    }

    public void updateserverresponse(Podcasts podcasts) {
        if (podcasts != null) {
            if (podcasts.getResponse().getPoadcastList().size() > 0) {
                this.podcasts = podcasts;
                tv_title.setText(podcasts.getResponse().getPoadcastList().get(position).getPodcastName());
                this.podcasts.getResponse().getPoadcastList().get(position).setPlaying(true);
                podcastadap = new podcastadapter(Podcast.this, this.podcasts);
                linearLayoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false);
                rv_podcast.setLayoutManager(linearLayoutManager);
                rv_podcast.setAdapter(podcastadap);
                progresslayout.setVisibility(View.GONE);
                try {
                    playfirstsong(podcasts.getResponse().getPoadcastList().get(0).getPodcastUrl());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        } else {
            NetworkChecker networkChecker = new NetworkChecker(Podcast.this);
            if (!networkChecker.isNetworkAvailable(this)) {
                networkChecker.showalertdialog();
            }
        }
    }

    @Override
    public void onBackPressed() {
        if (handler != null) {
//            handler.removeCallbacks(r);
//            handler=null;

            Stop();
        }
        if (progresshelper.getMediaplayer() != null) {
            progresshelper.getMediaplayer().release();
            progresshelper.setMediaplayer(null);
        }
        position = 0;
        if (intent != null) {
            stopService(intent);
            intent=null;
        }
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancel(1);
        super.onBackPressed();
        overridePendingTransition(0, 0);
    }

    @OnClick(R.id.iv_back)
    public void openmenu() {
        if (handler != null) {
            Stop();
        }
        if (progresshelper.getMediaplayer() != null) {
            progresshelper.getMediaplayer().release();
            progresshelper.setMediaplayer(null);
        }
        position = 0;
        if (intent != null) {
            stopService(intent);
            intent=null;
            NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.cancel(1);
        }

        super.onBackPressed();
        overridePendingTransition(0, 0);
    }

    @Override
    public void onDestroy() {
        if (progresshelper.getMediaplayer() != null) {
            progresshelper.getMediaplayer().release();
            progresshelper.setMediaplayer(null);
        }
        if (intent != null) {
            stopService(intent);
            intent=null;
            NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.cancel(1);
        }
        super.onDestroy();
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        if (fromUser) {
            seekBar.setProgress(progress);
            progresshelper.getMediaplayer().seekTo(progress * 1000);
        }
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {
    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
    }

    public void notifyadapter() {
        for (int i = 0; i < podcasts.getResponse().getPoadcastList().size(); i++) {
            if (!(i == position)) {
                podcasts.getResponse().getPoadcastList().get(i).setPlaying(false);
            } else {
                podcasts.getResponse().getPoadcastList().get(position).setPlaying(true);
            }
        }
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                podcastadap.notifyDataSetChanged();
            }
        });
    }

    public void clearadapter() {
        for (int i = 0; i < podcasts.getResponse().getPoadcastList().size(); i++) {
            podcasts.getResponse().getPoadcastList().get(i).setPlaying(false);
        }
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                podcastadap.notifyDataSetChanged();
            }
        });
    }


    @Override
    public boolean onError(MediaPlayer mp, int what, int extra) {
        return true;
    }

    @Override
    public void onCompletion(MediaPlayer mp) {
        if (!(status2 || status3)) {
            position = position + 1;
            if (position < podcasts.getResponse().getPoadcastList().size()) {
                tv_title.setText(podcasts.getResponse().getPoadcastList().get(position).getPodcastName());
                notifyadapter();
                changesong();
            } else {
                if (status4) {
                    position = 0;
                    tv_title.setText(podcasts.getResponse().getPoadcastList().get(position).getPodcastName());
                    notifyadapter();
                    changesong();
                }
            }
        } else {
            if (status2) {
                repeatPodcasts();
            } else if (status3) {
                shufflePodcasts();
            }
        }
    }

    @Override
    public void onBufferingUpdate(MediaPlayer mp, int percent) {

    }

    @Override
    public void onPrepared(MediaPlayer mp) {
        DecimalFormat decimalFormat = new DecimalFormat("00");
        String a = decimalFormat.format(((int) (progresshelper.getMediaplayer().getDuration() / 1000) / 60));
        String b = decimalFormat.format((progresshelper.getMediaplayer().getDuration() / 1000) % 60);
        tv_finaltime.setText(a + ":" + b + "");
        sb_podcast.setMax(progresshelper.getMediaplayer().getDuration() / 1000);
        mp.start();
        starthandler();
        intent = new Intent(this, NotificationHelper.class);
        startService(intent);
    }

    public void starthandler() {
        try {
            handler = new Handler();
            handler.postDelayed(r = new Runnable() {
                @Override
                public void run() {
                    progresshelper.setProgress();
                    handler.postDelayed(this, 0);
                }
            }, 0);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onSeekComplete(MediaPlayer mp) {

    }


    public void Stop() {
        handler.removeCallbacks(null);
    }

    @Override
    public boolean onInfo(MediaPlayer mp, int what, int extra) {
        return false;
    }


    //    public class Durations extends AsyncTask<String, String, String> {
//        @Override
//        protected void onPreExecute() {
//            super.onPreExecute();
//            if (position1 == 0) {
//                position=0;
//                podcastadap = new podcastadapter(Podcast.this,podcasts);
//                linearLayoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false);
//                rv_podcast.setLayoutManager(linearLayoutManager);
//                rv_podcast.setAdapter(podcastadap);
//                progresslayout.setVisibility(View.GONE);
//                tv_title.setText(podcasts.getResponse().getPoadcastList().get(position).getPodcastName());
//            } else {
//                podcastadap.notifyDataSetChanged();
//            }
//        }
//        @Override
//        protected String doInBackground(String... params) {
//            BufferedInputStream bis = new BufferedInputStream(fis);
//            AudioFile audioFile = AudioFileIO.read(file);
//            duration= audioFile.getAudioHeader().getTrackLength();
//
//            System.out.print("time in milliseconds=   "+duration)  ;
//
//            return "";
//        }
//        @Override
//        protected void onPostExecute(String s) {
//            super.onPostExecute(s);
//            position1++;
//            if (position1 <linearLayoutManager.getChildCount()) {
//                new Durations().executeOnExecutor(THREAD_POOL_EXECUTOR);
//            } else {
//                    position1 = 0;
//                    position=0;
//                    podcasts.getResponse().getPoadcastList().get(position).setPlaying(true);
//                    podcastadap.notifyDataSetChanged();
//                    return;
//            }
//        }
//    }
//    public void nopodcastsdialog() {
//        android.app.AlertDialog.Builder b = new android.app.AlertDialog.Builder(Podcast.this);
//        b.setMessage(this.getString(R.string.nopodcasts));
//        b.setTitle("Podcasts");
//        b.setCancelable(false);
//        b.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//                dialog.dismiss();
//                finish();
//                overridePendingTransition(0, 0);
//            }
//        });
//        b.show();
//    }
    public void refreshtitle() {
        tv_initialtime.setText("00:00");
        tv_finaltime.setText("00:00");
        tv_title.setText(podcasts.getResponse().getPoadcastList().get(position).getPodcastName());
    }

    @Override
    public void update(Observable o, Object arg) {
        DecimalFormat decimalFormat = new DecimalFormat("00");
        String a = decimalFormat.format(((int) (progresshelper.getProgress() / 1000) / 60));
        String b = decimalFormat.format((progresshelper.getProgress() / 1000) % 60);
        tv_initialtime.setText(a + ":" + b + "");
        sb_podcast.setProgress(progresshelper.getProgress() / 1000);
    }
}


