package com.example.root.ubn_radio.view.activities.view.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.root.ubn_radio.R;
import com.example.root.ubn_radio.view.activities.controllers.Heveletica_light;
import com.example.root.ubn_radio.view.activities.model.Listen_live.ListenLive;
import com.example.root.ubn_radio.view.activities.model.Watch_live.Watchlive;
import com.example.root.ubn_radio.view.activities.view.activities.PlayRadio;
import com.example.root.ubn_radio.view.activities.view.activities.Player;
import com.example.root.ubn_radio.view.activities.view.activities.SimplePlayer;
import com.squareup.picasso.Picasso;

import java.io.Serializable;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by root on 17/10/16.
 */

public class watchliveadapter extends RecyclerView.Adapter<watchliveadapter.viewholder> {
    Activity activity;
    Watchlive watchlive;

    public watchliveadapter(Activity activity, Watchlive watchlive) {
        this.activity = activity;
        this.watchlive = watchlive;
    }

    @Override
    public viewholder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater=(LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v=inflater.inflate(R.layout.watchliveitemlayout,parent,false);
        return new viewholder(v);
    }

    @Override
    public void onBindViewHolder(viewholder holder, final int position) {
        holder.tv_channelname.setText(watchlive.getResponse().getChannels().get(position).getChannelName());
        holder.rl_next_watchlive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.startActivity(new Intent(activity, SimplePlayer.class).putExtra("url",watchlive.getResponse().getChannels().get(position).getChannelStream()));
                activity.overridePendingTransition(0,0);
            }
        });
        if(!watchlive.getResponse().getChannels().get(position).getChannelThumbnailUrl().equals(""))
        Picasso.with(activity).load(watchlive.getResponse().getChannels().get(position).getChannelThumbnailUrl()).into(holder.iv_channel);
    }

    @Override
    public int getItemCount() {
        return watchlive.getResponse().getChannels().size();
    }

    public class viewholder extends RecyclerView.ViewHolder{
        CircleImageView iv_channel;
        Heveletica_light tv_channelname;
        RelativeLayout rl_next_watchlive;
        public viewholder(View itemView) {
            super(itemView);
            iv_channel=(CircleImageView) itemView.findViewById(R.id.iv_channel);
            tv_channelname=(Heveletica_light) itemView.findViewById(R.id.tv_channelname);
            rl_next_watchlive=(RelativeLayout) itemView.findViewById(R.id.rl_next_watchlive);
        }
    }
}
