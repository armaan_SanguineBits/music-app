package com.example.root.ubn_radio.view.activities.model.podcast;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Response {

@SerializedName("poadcast_list")
@Expose
private List<PoadcastList> poadcastList = new ArrayList<PoadcastList>();

/**
*
* @return
* The poadcastList
*/
public List<PoadcastList> getPoadcastList() {
return poadcastList;
}

/**
*
* @param poadcastList
* The poadcast_list
*/
public void setPoadcastList(List<PoadcastList> poadcastList) {
this.poadcastList = poadcastList;
}

}