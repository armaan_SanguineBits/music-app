package com.example.root.ubn_radio.view.activities.model.Listen_live;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


public class Response implements Serializable{

@SerializedName("channels")
@Expose
private List<Channel> channels = new ArrayList<Channel>();
@SerializedName("total_pages")
@Expose
private Integer totalPages;

/**
*
* @return
* The channels
*/
public List<Channel> getChannels() {
return channels;
}

/**
*
* @param channels
* The channels
*/
public void setChannels(List<Channel> channels) {
this.channels = channels;
}

/**
*
* @return
* The totalPages
*/
public Integer getTotalPages() {
return totalPages;
}

/**
*
* @param totalPages
* The total_pages
*/
public void setTotalPages(Integer totalPages) {
this.totalPages = totalPages;
}

}