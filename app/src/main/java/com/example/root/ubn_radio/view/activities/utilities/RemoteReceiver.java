package com.example.root.ubn_radio.view.activities.utilities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.content.LocalBroadcastManager;

import com.example.root.ubn_radio.view.activities.view.activities.Podcast;


/**
 * Created by root on 20/12/16.
 */

public class RemoteReceiver extends BroadcastReceiver {


    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equalsIgnoreCase("com.example.root.ubn_radio.TOGGLE_PLAYBACK")) {
            Podcast.getinstance().play();
            NotificationHelper.getinstance().buildnotification(!Podcast.status);
           /* Intent i = new Intent("com.example.root.ubn_radio.mediaCONTROL");
            LocalBroadcastManager.getInstance(context).sendBroadcast(i);*/
        }
        if (intent.getAction().equalsIgnoreCase("com.example.root.ubn_radio.PREV")) {
            Podcast.getinstance().rewind();
        }
        if (intent.getAction().equalsIgnoreCase("com.example.root.ubn_radio.NEXT")) {
            Podcast.getinstance().forward();
        }
    }
}
