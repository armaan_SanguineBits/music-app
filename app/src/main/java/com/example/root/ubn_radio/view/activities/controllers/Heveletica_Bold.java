package com.example.root.ubn_radio.view.activities.controllers;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by root on 25/11/16.
 */

public class Heveletica_Bold extends TextView {
    public Heveletica_Bold(Context context, AttributeSet attrs) {
        super(context, attrs);
        setTypeface(Typeface.createFromAsset(context.getAssets(), "Helvetica Narrow Bold.ttf"));
    }
}
