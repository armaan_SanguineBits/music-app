package com.example.root.ubn_radio.view.activities.view.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import com.example.root.ubn_radio.R;
import com.example.root.ubn_radio.view.activities.controllers.Heveletica_light;
import com.example.root.ubn_radio.view.activities.model.podcast.Podcasts;
import com.example.root.ubn_radio.view.activities.view.activities.Podcast;

/**
 * Created by root on 17/10/16.
 */

public class podcastadapter extends RecyclerView.Adapter<podcastadapter.viewholder> {
    Activity activity;
    Podcasts podcasts;
    public podcastadapter(Activity activity, Podcasts podcasts){
        this.activity = activity;
        this.podcasts = podcasts;
    }
    @Override
    public viewholder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater=(LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v=inflater.inflate(R.layout.podcastitemlayout,parent,false);
        return new viewholder(v);
    }
    @Override
    public void onBindViewHolder(final viewholder holder, final int position) {
        if(podcasts.getResponse().getPoadcastList().get(position).isPlaying()) {
            holder.iv_play.setImageDrawable(activity.getResources().getDrawable(R.drawable.small_pause));
            holder.rl_podcast.setEnabled(false);
        } else {
            holder.iv_play.setImageDrawable(activity.getResources().getDrawable(R.drawable.small_play));
            holder.rl_podcast.setEnabled(true);
        }
        holder.tv_time.setText(podcasts.getResponse().getPoadcastList().get(position).getDuration());
        holder.tv_show_title.setText(podcasts.getResponse().getPoadcastList().get(position).getPodcastName());
        holder.rl_podcast.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Podcast.position=position;
                Podcast.getinstance().changesong();
                for(int i=0;i<Podcast.podcasts.getResponse().getPoadcastList().size();i++){
                    if(!(i==position)){
                        Podcast.podcasts.getResponse().getPoadcastList().get(i).setPlaying(false);
                    }else{
                        Podcast.podcasts.getResponse().getPoadcastList().get(position).setPlaying(true);
                    }
                }
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Podcast.getinstance().refreshtitle();
                        Podcast.podcastadap.notifyDataSetChanged();
                    }
                });
            }
        });
    }


    @Override
    public int getItemCount() {
        return podcasts.getResponse().getPoadcastList().size();
    }

    public class viewholder extends RecyclerView.ViewHolder{
        RelativeLayout rl_podcast;
        ImageView iv_play;
        Heveletica_light tv_show_title;
        Heveletica_light tv_time;
        public viewholder(View itemView) {
            super(itemView);
            rl_podcast=(RelativeLayout)itemView.findViewById(R.id.relative_podcast);
            iv_play=(ImageView) itemView.findViewById(R.id.iv_play);
            tv_show_title=(Heveletica_light) itemView.findViewById(R.id.tv_title);
            tv_time=(Heveletica_light) itemView.findViewById(R.id.tv_time);
        }
    }
}
