package com.example.root.ubn_radio.view.activities.application;

import android.app.Application;
import com.crashlytics.android.Crashlytics;
import com.example.root.ubn_radio.view.activities.utilities.AudioFocusHelper;
import com.example.root.ubn_radio.view.activities.view.activities.Podcast;

import io.fabric.sdk.android.Fabric;

/**
 * Created by root on 17/10/16.
 */

public class App extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());
    }
}
