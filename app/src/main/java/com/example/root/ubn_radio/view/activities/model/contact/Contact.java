package com.example.root.ubn_radio.view.activities.model.contact;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Contact {

@SerializedName("response")
@Expose
private String response;

public String getResponse() {
return response;
}

public void setResponse(String response) {
this.response = response;
}

}