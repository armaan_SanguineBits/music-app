package com.example.root.ubn_radio.view.activities.utilities;
import android.media.MediaPlayer;
import java.util.Observable;

/**
 * Created by root on 16/12/16.
 */

public class ProgressHelper extends Observable {
    MediaPlayer mediaplayer ;
    int progress;

    public void setProgress() {
        try {
            this.progress = mediaplayer.getCurrentPosition();
            setChanged();
            notifyObservers();
        }catch (Exception e)
        {
            e.printStackTrace();
        }

    }

    public void setMediaplayer(MediaPlayer mediaplayer) {
        this.mediaplayer = mediaplayer;
    }

    public MediaPlayer getMediaplayer() {
        return mediaplayer;
    }

    public int getProgress() {
        return progress;
    }
}
