package com.example.root.ubn_radio.view.activities.utilities;

import android.annotation.TargetApi;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.ComponentName;
import android.content.Intent;
import android.graphics.Color;
import android.media.MediaMetadata;
import android.media.session.MediaSession;
import android.os.Build;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v7.app.NotificationCompat;
import android.widget.Toast;

import com.example.root.ubn_radio.R;

/**
 * Created by root on 15/12/16.
 */

public class NotificationHelper extends Service {
    private static final String ACTION_TOGGLE_PLAYBACK = "com.example.root.ubn_radio.TOGGLE_PLAYBACK";
    private static final String ACTION_PREV = "com.example.root.ubn_radio.PREV";
    private static final String ACTION_NEXT = "com.example.root.ubn_radio.NEXT";
    private static NotificationHelper instance;
    private NotificationManager notificationmanager;
    public static Notification noti;
    private static MediaSession mediaSession;
    @Override
    public void onStart(Intent intent, int startId) {
        super.onStart(intent, startId);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        buildnotification(false);
    }

    public static NotificationHelper getinstance() {
        return instance;
    }


    public  void buildnotification(boolean value) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            if (mediaSession == null){
                mediaSession = new MediaSession(getApplicationContext(), "debug tag");
            }
            mediaSession.setMetadata(new MediaMetadata.Builder().putString(MediaMetadata.METADATA_KEY_TITLE, "").build());
            mediaSession.setActive(true);
            mediaSession.setCallback(new MediaSession.Callback() {
            });
            mediaSession.setFlags(MediaSession.FLAG_HANDLES_TRANSPORT_CONTROLS);

            noti = new Notification.Builder(getApplicationContext())
                    .setShowWhen(false)
                    .setStyle(new Notification.MediaStyle()
                            .setMediaSession(mediaSession.getSessionToken())
                            .setShowActionsInCompactView(0, 1, 2))
                    .setContentTitle("UBN Radio")
                    .setColor(Color.parseColor("#275080"))
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .addAction(R.drawable.forward, "prev", retreivePlaybackAction(3))
                    .addAction(value ? R.drawable.play_btn : R.drawable.pause_btn, "pause", retreivePlaybackAction(1))
                    .addAction(R.drawable.next, "next", retreivePlaybackAction(2)).setAutoCancel(false)
                    .setOngoing(true)
                    .build();

            notificationmanager = (NotificationManager) getApplication().getSystemService(NOTIFICATION_SERVICE);
            notificationmanager.notify(1, noti);
        }else{
                noti = new Notification.Builder(getApplicationContext())
                        .setContentTitle("UBN Radio")
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .addAction(R.drawable.forward, "prev", retreivePlaybackAction(3))
                        .addAction(value ? R.drawable.play_btn : R.drawable.pause_btn, "pause", retreivePlaybackAction(1))
                        .addAction(R.drawable.next, "next", retreivePlaybackAction(2)).setAutoCancel(false)
                        .setOngoing(true)
                        .build();
            notificationmanager = (NotificationManager) getApplication().getSystemService(NOTIFICATION_SERVICE);
            notificationmanager.notify(1, noti);
        }
    }

    private PendingIntent retreivePlaybackAction(int which) {
        Intent action;
        PendingIntent pendingIntent;
        final ComponentName serviceName = new ComponentName(this, RemoteReceiver.class);
        switch (which) {
            case 1:
                action = new Intent(ACTION_TOGGLE_PLAYBACK);
                action.setComponent(serviceName);
                pendingIntent = PendingIntent.getBroadcast(getApplicationContext(), 1, action, 0);
                return pendingIntent;
            case 2:
                action = new Intent(ACTION_NEXT);
                action.setComponent(serviceName);
                pendingIntent = PendingIntent.getBroadcast(getApplicationContext(), 2, action, 0);
                return pendingIntent;
            case 3:
                action = new Intent(ACTION_PREV);
                action.setComponent(serviceName);
                pendingIntent = PendingIntent.getBroadcast(getApplicationContext(), 3, action, 0);
                return pendingIntent;
            default:
                break;
        }
        return null;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
