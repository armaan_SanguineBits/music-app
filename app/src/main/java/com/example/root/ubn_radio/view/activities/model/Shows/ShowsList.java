package com.example.root.ubn_radio.view.activities.model.Shows;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;


public class ShowsList implements Serializable {

@SerializedName("id")
@Expose
private String id;
@SerializedName("show_title")
@Expose
private String showTitle;
@SerializedName("show_desc")
@Expose
private String showDesc;
@SerializedName("thumbnail_url")
@Expose
private String thumbnailUrl;

/**
*
* @return
* The id
*/
public String getId() {
return id;
}

/**
*
* @param id
* The id
*/
public void setId(String id) {
this.id = id;
}

/**
*
* @return
* The showTitle
*/
public String getShowTitle() {
return showTitle;
}

/**
*
* @param showTitle
* The show_title
*/
public void setShowTitle(String showTitle) {
this.showTitle = showTitle;
}

/**
*
* @return
* The showDesc
*/
public String getShowDesc() {
return showDesc;
}

/**
*
* @param showDesc
* The show_desc
*/
public void setShowDesc(String showDesc) {
this.showDesc = showDesc;
}

/**
*
* @return
* The thumbnailUrl
*/
public String getThumbnailUrl() {
return thumbnailUrl;
}

/**
*
* @param thumbnailUrl
* The thumbnail_url
*/
public void setThumbnailUrl(String thumbnailUrl) {
this.thumbnailUrl = thumbnailUrl;
}

}