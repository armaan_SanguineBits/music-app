package com.example.root.ubn_radio.view.activities.view.activities;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.root.ubn_radio.R;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class Social extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_social);
        ButterKnife.bind(this);
    }
    @OnClick(R.id.rl_next_facebook)
    public void openFacebook(){
        startActivity(new Intent(Intent.ACTION_VIEW,Uri.parse("https://www.facebook.com/UniversalBroadcastingNetwork")));
    }

    @OnClick(R.id.rl_next_twitter)
    public void openTwitter(){
        startActivity(new Intent(Intent.ACTION_VIEW,Uri.parse("https://twitter.com/UBNRADIOTV")));
    }
    @OnClick(R.id.rl_next_youtube)
    public void openYoutube(){
        startActivity(new Intent(Intent.ACTION_VIEW,Uri.parse("https://www.youtube.com/user/SuperUBN")));
    }
    @OnClick(R.id.iv_back)
    public void openMenu(){
        super.onBackPressed();
        overridePendingTransition(0,0);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransition(0,0);
    }
}
