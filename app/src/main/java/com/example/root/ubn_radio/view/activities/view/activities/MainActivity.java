package com.example.root.ubn_radio.view.activities.view.activities;

import android.app.Notification;
import android.app.NotificationManager;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import com.example.root.ubn_radio.R;
import com.example.root.ubn_radio.view.activities.utilities.NotificationHelper;

public class MainActivity extends AppCompatActivity {
    ImageView ivSplash;
    ImageView ivBack;
    ImageView iv_listenLive;
    ImageView iv_watchLive;
    ImageView iv_shows;
    ImageView iv_news;
    ImageView iv_social;
    ImageView iv_contact;
    View viewSplash;
    LinearLayout toolLayout;
    private View viewMain;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewSplash=getLayoutInflater().inflate(R.layout.layout_splash,null);
        viewMain=getLayoutInflater().inflate(R.layout.activity_main,null);
        setContentView(viewSplash);
        viewSplash.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        ivSplash=(ImageView)findViewById(R.id.iv_splash);
        ivSplash.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewSplash.startAnimation(AnimationUtils.loadAnimation(MainActivity.this,R.anim.left_out));
                setContentView(viewMain);
                iv_listenLive=(ImageView) findViewById(R.id.iv_listenLive);
                iv_watchLive=(ImageView) findViewById(R.id.iv_watchLive);
                iv_shows=(ImageView) findViewById(R.id.iv_shows);
                iv_news=(ImageView) findViewById(R.id.iv_news);
                iv_social=(ImageView) findViewById(R.id.iv_social);
                iv_contact=(ImageView) findViewById(R.id.iv_contact);
                ivBack=(ImageView) findViewById(R.id.iv_back);
                toolLayout=(LinearLayout) findViewById(R.id.toolLayout);
                toolLayout.startAnimation(AnimationUtils.loadAnimation(MainActivity.this,R.anim.slide_down));
                iv_listenLive.setOnTouchListener(new View.OnTouchListener() {
                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        if(event.getAction()==MotionEvent.ACTION_DOWN){
                            int color = Color.parseColor("#808080");
                            iv_listenLive.setColorFilter(color);
                        }else if(event.getAction()==MotionEvent.ACTION_UP){
                            int color = Color.WHITE;
                            iv_listenLive.setColorFilter(color);
                            startActivity(new Intent(MainActivity.this,LIsten_Live.class));
                            overridePendingTransition(0,0);
                        }
                        return true;
                    }
                });
                iv_watchLive.setOnTouchListener(new View.OnTouchListener() {
                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        if(event.getAction()==MotionEvent.ACTION_DOWN){
                            int color = Color.parseColor("#808080");
                            iv_watchLive.setColorFilter(color);
                        }else if(event.getAction()==MotionEvent.ACTION_UP){
                            int color = Color.WHITE;
                            iv_watchLive.setColorFilter(color);
                            startActivity(new Intent(MainActivity.this,Watch_LIve.class));
                            overridePendingTransition(0,0);
                        }
                        return true;
                    }
                });
                iv_shows.setOnTouchListener(new View.OnTouchListener() {
                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        if(event.getAction()==MotionEvent.ACTION_DOWN){
                            int color = Color.parseColor("#808080");
                            iv_shows.setColorFilter(color);
                        }else if(event.getAction()==MotionEvent.ACTION_UP){
                            int color = Color.WHITE;
                            iv_shows.setColorFilter(color);
                            startActivity(new Intent(MainActivity.this,Shows.class));
                            overridePendingTransition(0,0);
                        }
                        return true;
                    }
                });
                iv_news.setOnTouchListener(new View.OnTouchListener() {
                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        if(event.getAction()==MotionEvent.ACTION_DOWN){
                            int color = Color.parseColor("#808080");
                            iv_news.setColorFilter(color);
                        }else if(event.getAction()==MotionEvent.ACTION_UP){
                            int color = Color.WHITE;
                            iv_news.setColorFilter(color);
                            startActivity(new Intent(MainActivity.this,Latestnews.class));
                            overridePendingTransition(0,0);
                        }
                        return true;
                    }
                });
                iv_social.setOnTouchListener(new View.OnTouchListener() {
                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        if(event.getAction()==MotionEvent.ACTION_DOWN){
                            int color = Color.parseColor("#808080");
                            iv_social.setColorFilter(color);
                        }
                        else if(event.getAction()==MotionEvent.ACTION_UP){
                            int color = Color.WHITE;
                            iv_social.setColorFilter(color);
                            startActivity(new Intent(MainActivity.this,Social.class));
                            overridePendingTransition(0,0);
                        }
                        return true;
                    }
                });
                iv_contact.setOnTouchListener(new View.OnTouchListener() {
                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        if(event.getAction()==MotionEvent.ACTION_DOWN){
                            int color = Color.parseColor("#808080");
                            iv_contact.setColorFilter(color);
                        }else if(event.getAction()==MotionEvent.ACTION_UP){
                            int color = Color.WHITE;
                            iv_contact.setColorFilter(color);
                            startActivity(new Intent(MainActivity.this,Contact.class));
                            overridePendingTransition(0,0);
                        }
                        return true;
                    }
                });
                ivBack.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        try{
                            addContentView(viewSplash,new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
                            viewSplash.startAnimation(AnimationUtils.loadAnimation(MainActivity.this,R.anim.left_in));
                            toolLayout.startAnimation(AnimationUtils.loadAnimation(MainActivity.this,R.anim.slide_up));
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                        }
                });
            }
        });
    }
}
