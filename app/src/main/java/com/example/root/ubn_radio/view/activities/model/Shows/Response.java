package com.example.root.ubn_radio.view.activities.model.Shows;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


public class Response implements Serializable {

@SerializedName("shows_list")
@Expose
private List<ShowsList> showsList = new ArrayList<ShowsList>();
@SerializedName("total_pages")
@Expose
private Integer totalPages;

/**
*
* @return
* The showsList
*/
public List<ShowsList> getShowsList() {
return showsList;
}

/**
*
* @param showsList
* The shows_list
*/
public void setShowsList(List<ShowsList> showsList) {
this.showsList = showsList;
}

/**
*
* @return
* The totalPages
*/
public Integer getTotalPages() {
return totalPages;
}

/**
*
* @param totalPages
* The total_pages
*/
public void setTotalPages(Integer totalPages) {
this.totalPages = totalPages;
}

}