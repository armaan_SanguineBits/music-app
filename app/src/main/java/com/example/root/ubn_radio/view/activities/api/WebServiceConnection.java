package com.example.root.ubn_radio.view.activities.api;

import retrofit.GsonConverterFactory;
import retrofit.Retrofit;

public class WebServiceConnection {

    public static final String baseUrl = "http://ubnradio.com/mobile/web_services/";
    public static WebServiceHolder holder;

    public WebServiceConnection() {
        init();
    }

    public void init() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        holder = retrofit.create(WebServiceHolder.class);
    }

    public static WebServiceConnection getInstance() {
        return new WebServiceConnection();
    }
}
