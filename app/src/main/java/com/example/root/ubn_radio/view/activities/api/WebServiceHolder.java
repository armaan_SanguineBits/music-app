package com.example.root.ubn_radio.view.activities.api;

import com.example.root.ubn_radio.view.activities.model.Listen_live.ListenLive;
import com.example.root.ubn_radio.view.activities.model.Shows.Shows;
import com.example.root.ubn_radio.view.activities.model.Watch_live.Watchlive;
import com.example.root.ubn_radio.view.activities.model.contact.Contact;
import com.example.root.ubn_radio.view.activities.model.podcast.Podcasts;

import retrofit.Call;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Query;

public interface WebServiceHolder {
    @GET("listen_live_channels.php")
    Call<ListenLive> getchannels(@Query("page")String page);

    @GET("shows_list.php")
    Call<Shows> getshows(@Query("page")String page);

    @GET("podcast_list.php")
    Call<Podcasts> getpodcasts(@Query("show_id")String showid);

    @GET("watch_live_channels.php")
    Call<Watchlive> getshowstowatch(@Query("page")String page);

    @GET("contact_us.php")
    Call<Contact> postmessage(@Query("name") String name, @Query("email") String email, @Query("subject") String subject, @Query("message") String message);
}