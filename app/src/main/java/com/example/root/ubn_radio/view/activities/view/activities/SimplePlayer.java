package com.example.root.ubn_radio.view.activities.view.activities;

import android.app.Activity;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.widget.MediaController;
import android.widget.Toast;
import android.widget.VideoView;

import com.example.root.ubn_radio.R;

public class SimplePlayer extends Activity {

    private VideoView videoview=null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_simple_player);
        videoview=(VideoView)findViewById(R.id.videoview);
        String s=getIntent().getStringExtra("url");
        videoview.setMediaController(new MediaController(this));
        videoview.setVideoURI(Uri.parse(getIntent().getStringExtra("url")));
        videoview.start();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        videoview=null;
    }
}
